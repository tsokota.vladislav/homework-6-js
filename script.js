// 1. Екранування - це заміна в тексті керуючих символів на відповідні текстові підстановки. 

// 2. Оголошення функції відбувається за допомогою: 
//  - Function declaration - має власне ім'я та її можна викликати в будь-якій частині коду
//  - Function expression - не має власного імені і записується в змінну. Її можна викликати тільки після оголошення в коді
//  - Named Function expression  - має власне ім'я (але не можна її викликати по цьому імені ) і записується в змінну.
//    Її можна викликати тільки після оголошення в коді.

// 3.  Hoisting - це механізм підняття при якому змінні та оголошення функції підіймаються вгору по своїй області видимості перед
// виконанням коду.     


 
function createNewUser () {
    let newUser = {};
    Object.defineProperties(newUser, {
        "firstName" : {
            value: prompt("Enter your name"),   
        },
        "lastName" : {
            value: prompt("Enter your last name"),   
        },
        "birthday" : {
            value: +prompt("Enter your birthday in the format dd.mm.yyyy").slice(-4),  
        },
        "getAge" : {
            get: function() {
                let today = new Date();
                let year = today.getFullYear();
                return year - newUser.birthday;
            },
        },
        "getPassword" : {
            get: function() {
                return newUser.firstName[0].toUpperCase() + newUser.lastName.toLowerCase() + newUser.birthday;
            },
        },
    });
    return newUser; 
};
let userNEW = createNewUser();
console.log(userNEW.getAge);
console.log(userNEW.getPassword);










 
 













